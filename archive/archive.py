import pathlib

from base64 import b64decode

def list_namespaces(wd):
    '''Returns list of namespaces'''
    namespaces = {}
    path = pathlib.Path(wd)
    for ns in path.iterdir():
        if ns.is_dir():
            ns_title = str(ns).split('/')[-1]
            namespaces[ns_title] = []
            for doc in ns.iterdir():
                if doc.is_dir():
                    doc_title = str(doc).split('/')[-1]
                    namespaces[ns_title].append(doc_title)

    return namespaces

def get_document(wd, namespace, title):
    '''Returns specific document from archive'''
    path = pathlib.Path(wd) / namespace / title / 'index.html'
    with path.open('r') as f:
        document = f.read()
    return document

def get_picture(wd, namespace, title, picture):
    '''Returns specific document from archive'''
    path = pathlib.Path(wd) / namespace / title / picture
    with path.open('rb') as f:
        picture = f.read()
    return picture

def add_document(wd, namespace, title, content):
    '''Adds new document to archive'''
    path = pathlib.Path(wd) / namespace / title
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)

    for c in content:
        file_path = path / c['name']
        with file_path.open('wb') as f:
            binary = c['data'].encode('utf-8')
            data = b64decode(binary)
            f.write(data)