# General Info

**Archive** is a server of solution architecture documentation.

# Development

## Run server in development mode

```
ARCHIVE_WORKING_DIR=~/archive FLASK_APP=app.py python -m flask run
```

## Tests

```
make test
```

## Build image

```
docker build --tag <username>/archive:<version> .
```

## Push image

```
docker push <username>/archive:<version>
```

# Usage

## Run server

```
docker run --name=archive --rm -d -v ~/archive:/archive -p 80:5000 <username>/archive:<version>
```