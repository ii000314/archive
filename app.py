import os

from flask import Flask, request, render_template

from archive import archive

working_dir = os.getenv('ARCHIVE_WORKING_DIR', '/archive')
app = Flask(__name__, instance_path=working_dir)

@app.route('/', methods=['GET'])
def show_welcome_page():
    namespaces = archive.list_namespaces(app.instance_path)
    return render_template('index.html', namespaces=namespaces)

@app.route('/<namespace>/<title>/', methods=['GET'])
def show_document_page(namespace, title):
    document = archive.get_document(app.instance_path, namespace, title)
    return document

@app.route('/<namespace>/<title>/<picture>', methods=['GET'])
def show_document_picture(namespace, title, picture):
    picture = archive.get_picture(app.instance_path, namespace, title, picture)
    return picture

@app.route('/<namespace>/<title>/', methods=['POST'])
def add_document_to_archive(namespace, title):
    archive.add_document(app.instance_path, namespace, title, request.json)
    return '', 201

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=int("5000"), debug=True)